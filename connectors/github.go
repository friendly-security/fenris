package connectors

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/friendly-security/fenris/domain"
	"gitlab.com/friendly-security/fenris/lib"
)

type GithubUser struct {
	Login string
}

type Credentials struct {
	Username string
	Token    string
}

func (c Credentials) isComplete() bool {
	return c.Username != "" && c.Token != ""
}

type GithubPullRequest struct {
	Url       string     `json:"url"`
	Id        int64      `json:"id"`
	HtmlUrl   string     `json:"html_url"`
	User      GithubUser `json:"user"`
	State     string     `json:"state"`
	CreatedAt time.Time  `json:"created_at"`
	ClosedAt  time.Time  `json:"closed_at"`
	MergedAt  time.Time  `json:"merged_at"`
}

func (g GithubPullRequest) MapToMergeRequest() domain.MergeRequest {
	return domain.MergeRequest{CreatedBy: g.User.Login, CreatedAt: g.CreatedAt, ClosedAt: g.ClosedAt, MergedAt: g.MergedAt, Location: g.HtmlUrl}
}

func GetGithubMergeRequests(owner string, pid string, creds Credentials, after time.Time) []GithubPullRequest {

	client := http.Client{}

	var botRequests = make([]GithubPullRequest, 0)
	var next = fmt.Sprintf("https://api.github.com/repos/%s/%s/pulls?state=all&sort=created&direction=desc&per_page=100&page=1", owner, pid)

	for {
		res, nextUrl := getPaginatedGithubResults(next, client, creds, after)
		next = nextUrl // Figure out why this is needed as opposed to res,next :=...
		botRequests = append(botRequests, res...)
		if next == "" {
			break
		}
	}

	return botRequests
}

func getPaginatedGithubResults(url string, client http.Client, creds Credentials, after time.Time) ([]GithubPullRequest, string) {
	log.Info("Now querying:", url)
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Accept", "application/vnd.github.v3+json")
	if creds.isComplete() {
		req.SetBasicAuth(creds.Username, creds.Token)
	}
	res, _ := client.Do(req)

	content, _ := io.ReadAll(res.Body)
	if res.StatusCode != 200 {
		// TODO improve this
		log.Error(string(content))
	}

	var pullRequests []GithubPullRequest
	json.Unmarshal(content, &pullRequests)

	botRequests := lib.Filter(pullRequests, botFilter)

	linkHeader := ParseLinkHeader(res.Header.Get("link"))

	if len(botRequests) > 0 && botRequests[len(botRequests)-1].CreatedAt.Before(after) {
		// Fuzzy after is good enough for me for now. If needed this can be made more precise
		return botRequests, ""
	} else if val, ok := linkHeader["next"]; ok {
		return botRequests, val.Url
	}

	// TODO error handling

	return botRequests, ""
}

func botFilter(g GithubPullRequest) bool {
	return g.User.Login == "renovate[bot]" || g.User.Login == "dependabot[bot]"
}
