package connectors

import (
	"reflect"
	"testing"
)

var linkHeaderFirstPage = `<https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=2>; rel="next", <https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=14>; rel="last"`
var linkHeaderIntermediatePage = `<https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=7>; rel="prev", <https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=9>; rel="next", <https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=14>; rel="last", <https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=1>; rel="first"`
var linkHeaderLastPage = `<https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=13>; rel="prev", <https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=1>; rel="first"`

func TestFirstPage(t *testing.T) {
	res := ParseLinkHeader(linkHeaderFirstPage)
	expected := map[string]Link{
		"next": {Url: "https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=2", Rel: "next"},
		"last": {Url: "https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=14", Rel: "last"},
	}

	if !reflect.DeepEqual(res, expected) {
		t.Fatal("Expected maps to be equal but they where not.")
	}
}

func TestLastPage(t *testing.T) {
	res := ParseLinkHeader(linkHeaderLastPage)
	expected := map[string]Link{
		"first": {Url: "https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=1", Rel: "first"},
		"prev":  {Url: "https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=13", Rel: "prev"},
	}

	if !reflect.DeepEqual(res, expected) {
		t.Fatal("Expected maps to be equal but they where not.")
	}
}

func TestIntermediatePage(t *testing.T) {
	res := ParseLinkHeader(linkHeaderIntermediatePage)
	expected := map[string]Link{
		"first": {Url: "https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=1", Rel: "first"},
		"prev":  {Url: "https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=7", Rel: "prev"},
		"next":  {Url: "https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=9", Rel: "next"},
		"last":  {Url: "https://api.github.com/repositories/193146567/pulls?state=all&sort=created&direction=desc&per_page=100&page=14", Rel: "last"},
	}

	if !reflect.DeepEqual(res, expected) {
		t.Fatal("Expected maps to be equal but they where not.")
	}
}
