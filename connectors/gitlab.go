package connectors

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/friendly-security/fenris/domain"
)

type GitlabUser struct {
	Name     string `json:"name"`
	Username string `json:"username"`
}
type GitlabMergeRequest struct {
	Url       string     `json:"web_url"`
	State     string     `json:"state"`
	Author    GitlabUser `json:"author"`
	CreatedAt time.Time  `json:"created_at"`
	ClosedAt  time.Time  `json:"closed_at"`
	MergedAt  time.Time  `json:"merged_at"`
}

func (g GitlabMergeRequest) MapToMergeRequest() domain.MergeRequest {
	return domain.MergeRequest{CreatedBy: g.Author.Name, CreatedAt: g.CreatedAt, ClosedAt: g.ClosedAt, MergedAt: g.MergedAt, Location: g.Url}
}

func GetGitlabMergeRequests(projectId string, accessToken string, botUsername string, after time.Time) []GitlabMergeRequest {

	client := http.Client{}
	var botRequests = make([]GitlabMergeRequest, 0)
	var next = fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/merge_requests?state=all&author_username=%s&order_by=created_at&sort=desc&per_page=100", projectId, botUsername)

	for {
		res, nextUrl := getPaginatedGitlabResults(next, client, accessToken, after)
		next = nextUrl
		botRequests = append(botRequests, res...)
		if next == "" {
			break
		}
	}
	return botRequests
}

func getPaginatedGitlabResults(url string, client http.Client, aT string, after time.Time) ([]GitlabMergeRequest, string) {
	log.Info("Now querying:", url)
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", aT))

	res, _ := client.Do(req)

	content, _ := io.ReadAll(res.Body)

	if res.StatusCode != 200 {
		// TODO improve this
		log.Error(string(content))
	}

	var botRequests = make([]GitlabMergeRequest, 0)

	json.Unmarshal(content, &botRequests)

	linkHeader := ParseLinkHeader(res.Header.Get("link"))

	if len(botRequests) > 0 && botRequests[len(botRequests)-1].CreatedAt.Before(after) {
		// Fuzzy after is good enough for me for now. If needed this can be made more precise
		return botRequests, ""
	} else if val, ok := linkHeader["next"]; ok {
		return botRequests, val.Url
	}

	// TODO error handling

	return botRequests, ""

}
