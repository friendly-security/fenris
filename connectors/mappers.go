package connectors

import (
	"gitlab.com/friendly-security/fenris/domain"
)

type MappableMergeRequest interface {
	MapToMergeRequest() domain.MergeRequest
}

func MapToMrs[MMR MappableMergeRequest](g []MMR) []domain.MergeRequest {
	res := []domain.MergeRequest{}

	for _, m := range g {
		res = append(res, m.MapToMergeRequest())
	}

	return res
}
