package connectors

import "strings"

type Link struct {
	Url string
	Rel string
}

func ParseLinkHeader(raw string) map[string]Link {
	var links = map[string]Link{}

	for _, chunk := range strings.Split(raw, ",") {
		link := Link{Url: "", Rel: ""}
		for _, piece := range strings.Split(chunk, ";") {
			piece = strings.Trim(piece, " ")
			if piece == "" {
				continue
			}
			if piece[0] == '<' && piece[len(piece)-1] == '>' {
				link.Url = strings.Trim(piece, "<>")
				continue
			}
			if strings.HasPrefix(piece, "rel") {
				link.Rel = strings.Trim(strings.Split(piece, "=")[1], "\"")
			}
		}
		if link.Url != "" {
			links[link.Rel] = link
		}
	}
	return links
}
