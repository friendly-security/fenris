package domain

import (
	"errors"
	"time"

	"gitlab.com/friendly-security/fenris/lib"
)

func CalculateResultStatistics(mrs []MergeRequest, t time.Duration) ResultStatistics {
	good := 0

	for _, m := range mrs {
		if m.OpenFor() <= t {
			good++
		}
	}

	return ResultStatistics{BotRequests: len(mrs), ResolvedInTime: good, NotResolvedInTime: len(mrs) - good, ResolvedInTimePercentage: float64(good) / float64(len(mrs)) * 100}

}

func CalculateDescriptiveStatistics(mrs []MergeRequest) (DescriptiveStatistics, error) {
	if len(mrs) == 0 {
		return DescriptiveStatistics{}, errors.New("can not calculate statistics on empty MR list")
	}
	openTimes := []float64{}

	max := 0.0
	min := mrs[0].OpenFor().Minutes()

	for _, m := range mrs {
		current := m.OpenFor().Minutes()

		if max < current {
			max = current
		}
		if min > current {
			min = current
		}

		openTimes = append(openTimes, current)
	}

	// No need to handle additional errors here
	//* Basically we will only get errors if we pass empty slices which we have already checked for
	avg, _ := lib.Mean(openTimes)
	med, _ := lib.Percentile(openTimes, 50.0)
	dev, _ := lib.StdDev(openTimes)
	p85, _ := lib.Percentile(openTimes, 85.0)
	p95, _ := lib.Percentile(openTimes, 95.0)
	p99, _ := lib.Percentile(openTimes, 99.0)

	return DescriptiveStatistics{Count: len(mrs), Min: min, Max: max, Avg: avg, Median: med, P85: p85, P95: p95, P99: p99, StdDev: dev}, nil
}
