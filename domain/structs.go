package domain

import (
	"time"
)

type MergeRequest struct {
	CreatedBy string
	CreatedAt time.Time
	ClosedAt  time.Time
	MergedAt  time.Time
	Location  string
}

func (m MergeRequest) IsOpen() bool {
	return m.ClosedAt.IsZero() && m.MergedAt.IsZero()
}

func (m MergeRequest) OpenFor() time.Duration {
	if !m.ClosedAt.IsZero() {
		return m.ClosedAt.Sub(m.CreatedAt)
	}
	if !m.MergedAt.IsZero() {
		return m.MergedAt.Sub(m.CreatedAt)
	}
	return -time.Until(m.CreatedAt)
}

type ResultStatistics struct {
	BotRequests              int
	ResolvedInTime           int
	NotResolvedInTime        int
	ResolvedInTimePercentage float64
}

type DescriptiveStatistics struct {
	Count  int
	Min    float64
	Max    float64
	Avg    float64
	StdDev float64
	Median float64
	P85    float64
	P95    float64
	P99    float64
}
