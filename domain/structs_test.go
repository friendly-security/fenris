package domain

import (
	"testing"
	"time"
)

func TestOpenMr(t *testing.T) {
	mr := MergeRequest{CreatedBy: "Renovate", CreatedAt: time.Now(), ClosedAt: time.Time{}, MergedAt: time.Time{}}

	if !mr.IsOpen() {
		t.Fatal("Expected MR to be open, but was closed")
	}
}

func TestClosedMr(t *testing.T) {
	mr := MergeRequest{CreatedBy: "Renovate", CreatedAt: time.Now(), ClosedAt: time.Now(), MergedAt: time.Time{}}

	if mr.IsOpen() {
		t.Fatal("Expected MR to be closed, but was open")
	}
}

func TestMergedMr(t *testing.T) {
	mr := MergeRequest{CreatedBy: "Renovate", CreatedAt: time.Now(), ClosedAt: time.Time{}, MergedAt: time.Now()}

	if mr.IsOpen() {
		t.Fatal("Expected MR to be closed, but was open")
	}
}
