# Fenris

A wolf that helps you hunting down and track projects burdened by maintenance load.

## Roadmap

- Use viper
- Add ability to run against multiple projects with one call
- Add connector for Influx as data sink
- Switch over to gitlab issues once the first MVP is out the door
