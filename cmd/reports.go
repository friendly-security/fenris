package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/friendly-security/fenris/connectors"
	"gitlab.com/friendly-security/fenris/domain"
)

var reportCmd = &cobra.Command{
	Use:   "reports",
	Short: "Generate and display reports",
}

var githubReport = &cobra.Command{
	Use:   "github",
	Short: "Generate report for github",
	Run:   generateGithubReport,
	PreRunE: func(cmd *cobra.Command, args []string) error {
		if (username != "" && token == "") || (username == "" && token != "") {
			return errors.New("username and token must always be provided together")
		}
		if owner == "" || projectId == "" {
			return errors.New("both owner and projectId must be provided")
		}
		return nil
	},
}

var githubThresholdReport = &cobra.Command{
	Use:   "github-threshold",
	Short: "Generate threshold report for github",
	Run:   thresholdReportGithub,
	PreRunE: func(cmd *cobra.Command, args []string) error {
		if (username != "" && token == "") || (username == "" && token != "") {
			return errors.New("username and token must always be provided together")
		}
		if owner == "" || projectId == "" {
			return errors.New("both owner and projectId must be provided")
		}
		return nil
	},
}

var gitlabReport = &cobra.Command{
	Use:   "gitlab",
	Short: "Generate report for gitlab",
	Run:   generateGitlabReport,
	PreRunE: func(cmd *cobra.Command, args []string) error {
		if projectId == "" {
			return errors.New("projectId must be provided")
		}
		if botName == "" {
			return errors.New("name of the user used by the bot must be provided")
		}
		return nil
	},
}

var reportFromConfig = &cobra.Command{
	Use:   "from-config",
	Short: "Generate a reports as per the configuration file",
	Run:   generateReportFromConfig,
}

var username string
var token string
var after string // Move to time.Time after https://github.com/spf13/pflag/pull/348
var threshold int64
var projectId string
var owner string
var botName string

func init() {
	rootCmd.AddCommand(reportCmd)
	reportCmd.AddCommand(githubReport)
	reportCmd.AddCommand(githubThresholdReport)
	reportCmd.AddCommand(gitlabReport)
	reportCmd.AddCommand(reportFromConfig)

	githubReport.Flags().StringVar(&username, "username", "", "A username for use with the Github API")
	githubReport.Flags().StringVar(&token, "token", "", "A token for use with the Github API")
	githubReport.Flags().StringVar(&after, "after", "", "Only consider MRs after roughly this date (defaults to now-90days)")
	githubReport.Flags().StringVar(&projectId, "projectId", "", "The id of the project to check")
	githubReport.Flags().StringVar(&owner, "owner", "", "The owner of the project to check")

	githubThresholdReport.Flags().StringVar(&username, "username", "", "A username for use with the Github API")
	githubThresholdReport.Flags().StringVar(&token, "token", "", "A token for use with the Github API")
	githubThresholdReport.Flags().StringVar(&after, "after", "", "Only consider MRs after roughly this date (defaults to now-90days)")
	githubThresholdReport.Flags().Int64Var(&threshold, "threshold", 24*7, "How long MRs are allowed to stay open before they are considered overdue. Provide value in hours (defaults to 7 days)")
	githubThresholdReport.Flags().StringVar(&projectId, "projectId", "", "The id of the project to check")
	githubThresholdReport.Flags().StringVar(&owner, "owner", "", "The owner of the project to check")

	gitlabReport.Flags().StringVar(&token, "token", "", "A token for use with the Github API")
	gitlabReport.Flags().StringVar(&projectId, "projectId", "", "The id of the project to check")
	gitlabReport.Flags().StringVar(&after, "after", "", "Only consider MRs after roughly this date (defaults to now-90days)")
	gitlabReport.Flags().StringVar(&botName, "botName", "", "The username of the user that creates the bots merge requests")
}

func generateGithubReport(cmd *cobra.Command, args []string) {
	afterTime := getPointInTimeOrDefault(after)

	gmrs := connectors.GetGithubMergeRequests(owner, projectId, connectors.Credentials{Username: username, Token: token}, afterTime)

	mrs := connectors.MapToMrs(gmrs)

	stat, _ := domain.CalculateDescriptiveStatistics(mrs)
	pretty, _ := json.MarshalIndent(stat, "", "\t")

	fmt.Println(string(pretty))

}

func generateGitlabReport(cmd *cobra.Command, args []string) {
	afterTime := getPointInTimeOrDefault(after)

	gmrs := connectors.GetGitlabMergeRequests(projectId, token, botName, afterTime)

	mrs := connectors.MapToMrs(gmrs)

	stat, _ := domain.CalculateDescriptiveStatistics(mrs)
	pretty, _ := json.MarshalIndent(stat, "", "\t")

	fmt.Println(string(pretty))

}

func thresholdReportGithub(cmd *cobra.Command, args []string) {
	afterTime := getPointInTimeOrDefault(after)

	gmrs := connectors.GetGithubMergeRequests(owner, projectId, connectors.Credentials{Username: username, Token: token}, afterTime)

	mrs := connectors.MapToMrs(gmrs)

	fmt.Println(domain.CalculateResultStatistics(mrs, time.Duration(hoursToNanoseconds(threshold))))

}

func generateReportFromConfig(cmd *cobra.Command, args []string) {
	// TODO
}

func hoursToNanoseconds(n int64) int64 {
	return n * 3600000000000
}

func getPointInTimeOrDefault(s string) time.Time {
	defaultInterval := time.Now().Add(-time.Hour * 24 * 90)

	if s == "" {
		return defaultInterval
	}

	res, err := time.Parse(time.RFC3339, s)
	if err != nil {
		log.Warn("Could not parse provided time. Must be RFC3339 compatible. Using default time")
		return defaultInterval
	}

	return res
}
