package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/friendly-security/fenris/lib"
)

func init() {
	rootCmd.AddCommand(showConfigCmd)
}

var showConfigCmd = &cobra.Command{
	Use:   "show-config",
	Short: "configuration",
	Run:   printConfig,
}

func printConfig(cmd *cobra.Command, args []string) {
	conf, err := lib.GetConfigurationFromFile()
	cobra.CheckErr(err)

	fmt.Println(conf)
}
