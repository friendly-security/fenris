package lib

/*
The code in this file is heavily inspired by the stats lib available at:
https://github.com/montanaflynn/stats which is licensed under the MIT License
*/

import (
	"errors"
	"math"
	"sort"
)

type FloatSlice []float64

func Mean(input FloatSlice) (float64, error) {
	if len(input) == 0 {
		return math.NaN(), errors.New("cannot calculate mean on empty slice")
	}

	return input.sum() / float64(len(input)), nil
}

func (fs FloatSlice) sum() float64 {
	sum := 0.0
	for _, v := range fs {
		sum += v
	}
	return sum
}

func (fs FloatSlice) sort() FloatSlice {
	res := make(FloatSlice, len(fs))
	copy(res, fs)
	sort.Float64s(res)
	return res
}

func Percentile(input FloatSlice, percent float64) (percentile float64, err error) {

	length := len(input)
	if length == 0 {
		return math.NaN(), errors.New("cannot calculate precentile on empty slice")
	}

	if percent <= 0 || percent > 100 {
		return math.NaN(), errors.New("requested percentage out of bounds (<=0 or >100)")
	}

	if length == 1 {
		return input[0], nil
	}

	sortedInput := input.sort()
	index := (percent / 100) * float64(len(sortedInput))

	if index == float64(int64(index)) {
		// index is a whole number
		i := int(index)
		percentile = sortedInput[i-1]
	} else if index > 1 {
		i := int(index)
		percentile, _ = Mean(FloatSlice{sortedInput[i-1], sortedInput[i]})
	} else {
		return math.NaN(), errors.New("calculation index out of bounds")
	}

	return percentile, nil

}

func StdDev(input FloatSlice) (stdDev float64, err error) {
	if len(input) == 0 {
		return math.NaN(), errors.New("cannot calculate standard deviation on empty slice")
	}

	vs, _ := variance(input, 0)
	return math.Sqrt(vs), nil
}

func variance(input FloatSlice, sample int) (variance float64, err error) {
	if len(input) == 0 {
		return math.NaN(), errors.New("cannot calculate variance on empty slice")
	}

	m, _ := Mean(input)

	for _, n := range input {
		variance += (n - m) * (n - m)
	}
	// The sample parameter will allow us to use this function to either calculate
	// the variance of the population or a sample
	return variance / float64(len(input)-(1*sample)), nil
}
