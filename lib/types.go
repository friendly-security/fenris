package lib

type gitlabConfig struct {
	BotName string
	Token   string
}
type gitlabProject struct {
	ProjectId string
}

type githubConfig struct {
	Username string
	Token    string
}
type githubProject struct {
	ProjectId string
	Owner     string
}

type projects struct {
	GitlabProjects []gitlabProject `mapstructure:"gitlab"`
	GithubProjects []githubProject `mapstructure:"github"`
}
type Configuration struct {
	Gitlab   gitlabConfig
	Github   githubConfig
	Projects projects
}
