package lib

import "github.com/spf13/viper"

func GetConfigurationFromFile() (Configuration, error) {
	var conf Configuration

	err := viper.Unmarshal(&conf)

	if err != nil {
		return Configuration{}, err
	}

	return conf, nil

}
