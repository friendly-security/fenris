package main

import (
	"gitlab.com/friendly-security/fenris/cmd"
)

func main() {
	/* #nosec */
	cmd.Execute()
}
