PROJECT_NAME := "fenris"
PKG := "gitlab.com/friendly-security/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint sec

all: build

lint: ## Lint the files
	@go vet ${PKG_LIST}

test: ## Run unittests
	@go test -short ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST} ## TODO install clang in CI

coverage: ## Generate global code coverage report
	./.helpers/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./.helpers/coverage.sh html;

dep: ## Get the dependencies
	@go install

sec: ## Perform secuirty checks
	@gosec ./...

build: dep ## Build the binary file
	@go build -v $(PKG)

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)
	@rm -rf .cover

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
